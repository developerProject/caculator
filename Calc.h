
#ifndef CALC_H
#define CALC_H
#include <string>

using namespace std;

class Calc{
	
	//access Specifer
	private:
		
		//Data member are not being used
		
		
		
	public:
			//MemberFunctions
		double add(double num1, double num2);
		double multiply(double num1, double num2);
		double divide(double num1, double num2);
		double subtract(double num1, double num2);
		string showChoice();
		
		
		
};

#endif
